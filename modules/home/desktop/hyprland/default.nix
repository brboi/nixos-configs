{
  lib,
  config,
  pkgs,
  namespace,
  ...
}: let
  inherit (lib) mkEnableOption mkIf;

  cfg = config.${namespace}.desktop.hyprland;
in {
  options.${namespace}.desktop.hyprland = {
    enable = mkEnableOption "Whether or not to enable hyprland WM home config.";
  };

  config = mkIf cfg.enable {
    xdg.configFile."hypr/" = {
      source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/.setup/modules/home/desktop/hyprland/config/";
      recursive = true;
    };
  };
}
