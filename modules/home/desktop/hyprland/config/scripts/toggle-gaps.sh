#!/usr/bin/env bash


function inner-toggle () {
  GAPS_IN=$(hyprctl -j getoption general:gaps_in | jq '.custom' | awk '{print $1}' | cut -c 2-)
  if [[ "$GAPS_IN" -eq "$1" ]]
  then
    hyprctl keyword general:gaps_in "$2"
  else
    hyprctl keyword general:gaps_in "$1"
  fi
}

function outer-toggle () {
  GAPS_OUT=$(hyprctl -j getoption general:gaps_out | jq '.custom' | awk '{print $1}' | cut -c 2-)
  if [[ "$GAPS_OUT" -eq "$1" ]]
  then
    hyprctl keyword general:gaps_out "$2"
  else
    hyprctl keyword general:gaps_out "$1"
  fi
}

while [[ $# -gt 0 ]]; do
  case $1 in
    --inner) inner-toggle "$2" "$3"; shift 3 ;;
    --outer) outer-toggle "$2" "$3"; shift 3 ;;
    *) printf "Error: Unknown option %s" "$1";  exit 1 ;;
  esac
done
