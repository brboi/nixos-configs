{
  lib,
  config,
  pkgs,
  namespace,
  ...
}: let
  inherit (lib) mkEnableOption mkIf;

  cfg = config.${namespace}.desktop.xsession;
in {
  options.${namespace}.desktop.xsession = {
    enable = mkEnableOption "Whether or not to enable xsession home config.";
  };

  config = mkIf cfg.enable {
    xsession = {
      enable = true;
      # ELECOM HUGE SCROLL CONFIG
      # -> Fn3 (button 12) becomes a scroll modifier + middle click
      initExtra = "
        xinput set-prop 'pointer:Getech HUGE TrackBall' 'libinput Button Scrolling Button' 12
        xinput set-prop 'pointer:Getech HUGE TrackBall' 'libinput Scroll Method Enabled' 0 0 1
        xinput set-button-map $(xinput list --id-only 'pointer:Getech HUGE TrackBall') \
          1 2 3 4 5 6 7 8 9 10 11 2
      ";
    };
  };
}
