# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, EzKey as Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
terminal = guess_terminal()

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key("M-h", lazy.layout.left(), desc="Move focus to left"),
    Key("M-l", lazy.layout.right(), desc="Move focus to right"),
    Key("M-j", lazy.layout.down(), desc="Move focus down"),
    Key("M-k", lazy.layout.up(), desc="Move focus up"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key("M-S-h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key(
        "M-S-l",
        lazy.layout.shuffle_right(),
        desc="Move window to the right",
    ),
    Key("M-S-j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key("M-S-k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key("M-C-h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key("M-C-l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key("M-C-j", lazy.layout.grow_down(), desc="Grow window down"),
    Key("M-C-k", lazy.layout.grow_up(), desc="Grow window up"),
    Key("M-n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        "M-S-<Return>",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key("M-<Return>", lazy.spawn(terminal), desc="Launch terminal"),
    Key("M-r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key(
        "M-<space>",
        lazy.spawn('rofi -show combi -modes combi -combi-modes "drun,window"'),
        desc="Launcher",
    ),
    # Toggle between different layouts as defined below
    Key("M-<Tab>", lazy.next_layout(), desc="Toggle between layouts"),
    Key("M-S-<Tab>", lazy.prev_layout(), desc="Toggle between layouts"),
    Key("M-S-c", lazy.window.kill(), desc="Kill focused window"),
    Key(
        "M-f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window",
    ),
    Key(
        "M-t",
        lazy.window.toggle_floating(),
        desc="Toggle floating on the focused window",
    ),
    Key("M-C-r", lazy.reload_config(), desc="Reload the config"),
    Key("M-C-q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key("<XF86MonBrightnessUp>", lazy.spawn("light -A 15"), desc="Brightness up"),
    Key("<XF86MonBrightnessDown>", lazy.spawn("light -U 15"), desc="Brightness down"),
    Key(
        "<XF86Launch3>", lazy.spawn("slock"), desc="Lock screen"
    ),  # fn+RShift on Lenovo Thinkpad
]

groups = [Group(i) for i in "1234567890"]
fr_groups = {  # For AZERTY FR
    "1": "<ampersand>",
    "2": "<eacute>",
    "3": "<quotedbl>",
    "4": "<apostrophe>",
    "5": "<parenleft>",
    "6": "<section>",
    "7": "<egrave>",
    "8": "<exclam>",
    "9": "<ccedilla>",
    "0": "<agrave>",
}

for i in groups:
    _key = i.name
    # _key = fr_groups[i.name] # For AZERTY FR
    keys.extend(
        [
            Key(
                f"M-{_key}",
                lazy.group[i.name].toscreen(toggle=True),
                desc="Switch to group {}".format(i.name),
            ),
            Key(
                f"M-S-{_key}",
                lazy.window.togroup(i.name, switch_group=True, toggle=True),
                desc="Move focused window to group {} and switch".format(i.name),
            ),
            Key(
                f"M-C-{_key}",
                lazy.screen.toggle_group(i.name, warp=True),
                desc="Toggle group {} on current screen".format(i.name),
            ),
        ]
    )

colors = [
    ["#282c34", "#282c34"],
    ["#1c1f24", "#1c1f24"],
    ["#dfdfdf", "#dfdfdf"],
    ["#ff6c6b", "#ff6c6b"],
    ["#98be65", "#98be65"],
    ["#da8548", "#da8548"],
    ["#51afef", "#51afef"],
    ["#c678dd", "#c678dd"],
    ["#46d9ff", "#46d9ff"],
    ["#a9a1e1", "#a9a1e1"],
]

layout_theme = {
    "border_width": 1,
    "margin": 2,
    "border_focus": "51afef",
    "border_normal": "1D2330",
}

layouts = [
    layout.Columns(
        **layout_theme,
        border_on_single=True,
    ),
    layout.Max(),
    layout.TreeTab(
        **layout_theme,
        active_bg=colors[6],
        active_fg=colors[2],
        inactive_bg=colors[1],
        inactive_fg=colors[2],
        urgent_bg=colors[3],
        urgent_fg=colors[2],
        bg_color=colors[0],
        fontshadow=colors[1],
        sections=[""],
        section_fontsize=0,
        panel_width=200,
        place_right=True,
    ),
]

widget_defaults = dict(
    font="Ubuntu Nerd Font", fontsize=10, padding=2, background=colors[2]
)
extension_defaults = widget_defaults.copy()

SEPARATOR_WIDGET = widget.Sep(
    linewidth=0,
    padding=6,
    foreground=colors[0],
    background=colors[0],
)


def make_bar(has_systray=False):
    return bar.Bar(
        [
            SEPARATOR_WIDGET,
            widget.GroupBox(
                font="Ubuntu Bold",
                fontsize=9,
                margin_y=3,
                margin_x=0,
                padding_y=5,
                padding_x=3,
                borderwidth=3,
                active=colors[2],
                inactive=colors[7],
                disable_drag=True,
                rounded=False,
                hide_unused=True,
                highlight_color=colors[1],
                highlight_method="line",
                this_current_screen_border=colors[6],
                this_screen_border=colors[4],
                other_current_screen_border=colors[6],
                other_screen_border=colors[4],
                foreground=colors[2],
                background=colors[0],
                urgent_alert_method="block",
                urgent_border=colors[3],
            ),
            widget.TextBox(
                text="|",
                font="Ubuntu Mono",
                background=colors[0],
                foreground="474747",
                padding=2,
                fontsize=14,
            ),
            SEPARATOR_WIDGET,
            widget.CurrentLayoutIcon(
                foreground=colors[2],
                background=colors[0],
                padding=0,
                scale=0.7,
            ),
            widget.CurrentLayout(
                foreground=colors[2],
                background=colors[0],
                padding=5,
            ),
            widget.TextBox(
                text="|",
                font="Ubuntu Mono",
                background=colors[0],
                foreground="474747",
                padding=2,
                fontsize=14,
            ),
            widget.Prompt(
                background=colors[0],
                foreground=colors[4],
            ),
            SEPARATOR_WIDGET,
            widget.WindowName(
                foreground=colors[6],
                background=colors[0],
                padding=0,
            ),
            widget.NetGraph(
                interface="auto",
                foreground=colors[3],
                background=colors[0],
                border_color=colors[0],
                graph_color=colors[7],
                fill_color=colors[7],
                border_width=1,
                line_width=1,
                type="box",
                samples=50,
            ),
        ]
        + (
            [
                widget.Systray(
                    background=colors[0],
                    padding=5,
                ),
            ]
            if has_systray
            else []
        )
        + [
            SEPARATOR_WIDGET,
            widget.Pomodoro(
                background=colors[0],
                padding=5,
                prefix_inactive="🍅",
            ),
            SEPARATOR_WIDGET,
            widget.ThermalSensor(
                foreground=colors[4],
                background=colors[0],
                threshold=90,
                fmt="Temp: {}",
                padding=5,
            ),
            widget.Clock(format="%a %d/%m %H:%M", background=colors[0]),
            SEPARATOR_WIDGET,
            widget.BatteryIcon(
                foreground=colors[0],
                background=colors[0],
                scale=1.5,
            ),
            widget.Battery(
                format="{percent:2.0%} ({hour:d}:{min:02d})",
                foreground=colors[2],
                background=colors[0],
            ),
            SEPARATOR_WIDGET,
        ],
        24,
    )


screens = [
    Screen(
        top=make_bar(has_systray=True),
        wallpaper="~/Pictures/wallpapers/Midgar8k.png",
        wallpaper_mode="fill",
    ),
    Screen(
        top=make_bar(),
        wallpaper="~/Pictures/wallpapers/Midgar8k.png",
        wallpaper_mode="fill",
    ),
    Screen(
        top=make_bar(),
        wallpaper="~/Pictures/wallpapers/Midgar8k.png",
        wallpaper_mode="fill",
    ),
]

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
