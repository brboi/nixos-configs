{
  lib,
  config,
  pkgs,
  ...
}: let
  inherit (lib) mkEnableOption mkIf;

  cfg = config.bobix.desktop.qtile;
in {
  options.bobix.desktop.qtile = {
    enable = mkEnableOption "Whether or not to enable qtile WM home config.";
  };

  config = mkIf cfg.enable {
    xdg.configFile.qtile = {
      source = config.lib.file.mkOutOfStoreSymlink "/home/${config.bobix.user.name}/.setup/modules/home/desktop/qtile/config";
      recursive = true;
    };
  };
}
