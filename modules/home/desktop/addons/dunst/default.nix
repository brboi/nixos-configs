{
  lib,
  config,
  pkgs,
  namespace,
  ...
}: let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.${namespace}.desktop.addons.dunst;
in {
  options.${namespace}.desktop.addons.dunst = {
    enable = mkEnableOption "Whether or not to enable dunst home config.";
  };

  config = mkIf cfg.enable {
    xdg.configFile.dunst = {
      source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/.setup/modules/home/desktop/addons/dunst/config";
      recursive = true;
    };
  };
}
