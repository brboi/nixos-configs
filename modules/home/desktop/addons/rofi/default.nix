{
  lib,
  config,
  pkgs,
  ...
}: let
  inherit (lib) mkEnableOption mkIf;

  cfg = config.bobix.desktop.addons.rofi;
in {
  options.bobix.desktop.addons.rofi = {
    enable = mkEnableOption "Whether or not to enable rofi home config.";
  };

  config = mkIf cfg.enable {
    xdg.configFile.rofi = {
      source = config.lib.file.mkOutOfStoreSymlink "/home/${config.bobix.user.name}/.setup/modules/home/desktop/addons/rofi/config";
      recursive = true;
    };
  };
}
