{
  lib,
  config,
  pkgs,
  namespace,
  ...
}: let
  inherit (lib) mkEnableOption mkIf;

  cfg = config.${namespace}.desktop.addons.waybar;
in {
  options.${namespace}.desktop.addons.waybar = {
    enable = mkEnableOption "Whether or not to enable waybar home config.";
  };

  config = mkIf cfg.enable {
    xdg.configFile."waybar/" = {
      source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/.setup/modules/home/desktop/addons/waybar/config/";
      recursive = true;
    };
  };
}
