{
  options,
  config,
  pkgs,
  lib,
  inputs,
  namespace,
  ...
}:
with lib;
with lib.${namespace}; let
  cfg = config.${namespace}.virtualisation.docker;
in {
  options.${namespace}.virtualisation.docker = with types; {
    enable = mkBoolOpt false "Whether or not to enable Docker.";
  };

  config = mkIf cfg.enable {
    ${namespace}.user.extraGroups = ["docker"];
    virtualisation.docker = {
      enable = true;
    };
    environment.systemPackages = with pkgs; [devcontainer];
  };
}
