{
  options,
  config,
  pkgs,
  lib,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.system.fonts;
in {
  options.bobix.system.fonts = with types; {
    enable = mkBoolOpt false "Whether or not to manage fonts.";
    fonts = mkOpt (listOf package) [] "Custom font packages to install.";
  };

  config = mkIf cfg.enable {
    environment.variables = {
      # Enable icons in tooling since we have nerdfonts.
      LOG_ICONS = "true";
    };

    environment.systemPackages = with pkgs; [font-manager];

    fonts.packages = with pkgs;
      [
        terminus_font
        font-awesome
        powerline-fonts
        powerline-symbols
        (nerdfonts.override {
          fonts = [
            "0xProto"
            "CascadiaCode" # CaskaydiaCove
            "FiraCode"
            "Hack"
            "Iosevka"
            "IosevkaTerm"
            "JetBrainsMono"
            "Mononoki"
            "Noto"
            "OpenDyslexic"
            "ProggyClean"
            "SourceCodePro" # SauceCodePro
            "Ubuntu"
            "UbuntuMono"
          ];
        })
      ]
      ++ cfg.fonts;
  };
}
