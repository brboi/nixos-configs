{
  options,
  config,
  pkgs,
  lib,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.system.console;
in {
  options.bobix.system.console = with types; {
    enable = mkBoolOpt false "Whether or not to enable console config.";
  };

  config = mkIf cfg.enable {
    boot.consoleLogLevel = 3;
    console = {
      earlySetup = true;
      font = mkDefault "${pkgs.terminus_font}/share/consolefonts/ter-u28n.psf.gz";
      keyMap = mkDefault "us";
      useXkbConfig = true; # needed if for instance there is a keymap variant
    };
  };
}
