{
  options,
  config,
  pkgs,
  lib,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.system.time;
in {
  options.bobix.system.time = with types; {
    enable =
      mkBoolOpt false "Whether or not to configure timezone information.";
  };

  config = mkIf cfg.enable {time.timeZone = "Europe/Brussels";};
}
