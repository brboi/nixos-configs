{
  options,
  config,
  lib,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.system.xkb;
in {
  options.bobix.system.xkb = with types; {
    enable = mkBoolOpt false "Whether or not to configure xkb.";
  };

  config = mkIf cfg.enable {
    services.xserver.xkb = {
      layout = mkDefault "us";
      variant = mkDefault "intl";
      options = mkDefault "caps:escape";
    };
  };
}
