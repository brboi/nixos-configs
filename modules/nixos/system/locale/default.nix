{
  options,
  config,
  pkgs,
  lib,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.system.locale;
  extraLocale = "fr_BE.UTF-8";
in {
  options.bobix.system.locale = with types; {
    enable = mkBoolOpt false "Whether or not to manage locale settings.";
  };

  config = mkIf cfg.enable {
    i18n = {
      defaultLocale = mkDefault "en_US.UTF-8";
      extraLocaleSettings = {
        LC_ADDRESS = mkDefault extraLocale;
        LC_IDENTIFICATION = mkDefault extraLocale;
        LC_MEASUREMENT = mkDefault extraLocale;
        LC_MONETARY = mkDefault extraLocale;
        LC_NAME = mkDefault extraLocale;
        LC_NUMERIC = mkDefault extraLocale;
        LC_PAPER = mkDefault extraLocale;
        LC_TELEPHONE = mkDefault extraLocale;
        LC_TIME = mkDefault extraLocale;
      };
    };
  };
}
