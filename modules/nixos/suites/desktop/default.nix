{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.suites.desktop;
in {
  options.bobix.suites.desktop = with types; {
    enable =
      mkBoolOpt false "Whether or not to enable common desktop configuration.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      alacritty
      appimage-run
      bitwarden
      gimp
      inkscape-with-extensions
      vlc
      gparted
      xorg.xev
    ];

    bobix = {
      apps = {
        belgian-eid = enabled;
        chromium = enabled;
        latex = enabled;
        librewolf = enabled;
        mailclient = enabled;
        office = enabled;
        pdfviewer = enabled;
        spotify = enabled;
      };

      desktop = {
        qtile = enabled;
        hyprland = enabled;
        addons = {
          arandr = enabled;
          battery-status = enabled;
          gtk = enabled;
          keyring = enabled;
          lockscreen = enabled;
          login-manager = enabled;
          nautilus = enabled;
          notifications = enabled;
          rofi = enabled;
          snipping-tool = enabled;
          system-monitor = enabled;
          unclutter = enabled;
          wallpapers = enabled;
        };
      };
    };
  };
}
