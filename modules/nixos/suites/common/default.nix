{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.suites.common;
in {
  options.bobix.suites.common = with types; {
    enable = mkBoolOpt false "Whether or not to enable common configuration.";
  };

  config = mkIf cfg.enable {
    services = {
      earlyoom = enabled;
      fstrim = enabled;
      fwupd = enabled;
      udisks2 = enabled;
    };

    programs = {
      light = enabled;
    };

    bobix = {
      hardware = {
        audio = enabled;
        elecom-huge = enabled;
        storage = enabled;
        networking = enabled;
      };

      nix = enabled;

      security = {
        doas = enabled;
        gpg = enabled;
        keyring = enabled;
        thetis = enabled;
        wireguard = enabled;
      };

      services = {
        avahi = enabled;
        printing = enabled;
      };

      system = {
        console = enabled;
        fonts = enabled;
        locale = enabled;
        time = enabled;
        xkb = enabled;
      };

      system.env = {
        # i.e. ENV_VAR_TO_SET = "value";
      };

      tools = {
        http = enabled;
        misc = enabled;
      };
    };
  };
}
