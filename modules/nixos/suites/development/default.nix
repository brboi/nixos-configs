{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace}; let
  cfg = config.${namespace}.suites.development;
in {
  options.${namespace}.suites.development = with types; {
    enable =
      mkBoolOpt false
      "Whether or not to enable common development configuration.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs;
    with pkgs.${namespace}; [
      neovim
      vscode
      tilp2 # from ${namespace}
    ];

    ${namespace} = {
      tools = {
        direnv = enabled;
        git = enabled;
        yarn = enabled;
      };
      virtualisation = {
        docker = enabled;
      };
    };
  };
}
