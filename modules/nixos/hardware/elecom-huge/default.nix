{
  options,
  config,
  pkgs,
  lib,
  namespace,
  ...
}:
with lib;
with lib.${namespace}; let
  cfg = config.${namespace}.hardware.elecom-huge;
in {
  options.${namespace}.hardware.elecom-huge = with types; {
    enable = mkBoolOpt false "Whether or not to enable remapping for Elecom HUGE trackballs.";
  };

  config = mkIf cfg.enable {
    services.xserver.config = "
      Section \"InputClass\"
          Identifier   \"Elecom HUGE scroll config\"
          MatchDriver  \"libinput\"
          MatchVendor  \"Getech\"
          MatchProduct \"HUGE TrackBall\"
          Option       \"ScrollMethod\" \"button\"
          Option       \"ScrollButton\" \"12\"
          Option       \"ButtonMapping\" \"1 2 3 4 5 6 7 8 9 10 11 2\"
      EndSection
    ";
  };
}
