{
  options,
  config,
  pkgs,
  lib,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.hardware.audio;
in {
  options.bobix.hardware.audio = with types; {
    enable = mkBoolOpt false "Whether or not to enable audio support.";
    extra-packages = mkOpt (listOf package) [
      pkgs.qjackctl
      pkgs.easyeffects
    ] "Additional packages to install.";
  };

  config = mkIf cfg.enable {
    services.pipewire = {
      enable = true;
      audio.enable = true;
      pulse.enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      jack.enable = true;
    };

    environment.systemPackages = with pkgs;
      [
        pwvucontrol
      ]
      ++ cfg.extra-packages;

    bobix.user.extraGroups = ["audio" "video"];
  };
}
