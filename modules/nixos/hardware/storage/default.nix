{
  options,
  config,
  pkgs,
  lib,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.hardware.storage;
in {
  options.bobix.hardware.storage = with types; {
    enable =
      mkBoolOpt false
      "Whether or not to enable support for extra storage devices.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ntfs3g fuseiso];
  };
}
