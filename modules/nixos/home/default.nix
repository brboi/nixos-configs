{
  options,
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.home;
in {
  imports = with inputs; [
    home-manager.nixosModules.home-manager
  ];

  options.bobix.home = with types; {
    file =
      mkOpt attrs {}
      "A set of files to be managed by home-manager's <option>home.file</option>.";
    configFile =
      mkOpt attrs {}
      "A set of files to be managed by home-manager's <option>xdg.configFile</option>.";
    extraOptions = mkOpt attrs {} "Options to pass directly to home-manager.";
  };

  config = {
    bobix.home.extraOptions = {
      home.stateVersion = config.system.stateVersion;
      home.file = mkAliasDefinitions options.bobix.home.file;
      xdg.enable = true;
      xdg.configFile = mkAliasDefinitions options.bobix.home.configFile;
    };

    home-manager = {
      useUserPackages = true;

      users.${config.bobix.user.name} =
        mkAliasDefinitions options.bobix.home.extraOptions;
    };
  };
}
