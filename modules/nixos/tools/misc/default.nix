{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.tools.misc;
in {
  options.bobix.tools.misc = with types; {
    enable = mkBoolOpt false "Whether or not to enable common utilities.";
  };

  config = mkIf cfg.enable {
    bobix.home.configFile."wgetrc".text = "";

    environment.systemPackages = with pkgs; [
      jq
      killall
      ncdu
      unzip
      wget
    ];
  };
}
