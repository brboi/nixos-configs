{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.tools.yarn;
in {
  options.bobix.tools.yarn = with types; {
    enable = mkBoolOpt false "Whether or not to enable yarn.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [pkgs.yarn];
  };
}
