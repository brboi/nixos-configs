{
  options,
  config,
  pkgs,
  lib,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.tools.git;
  user = config.bobix.user;
in {
  options.bobix.tools.git = with types; {
    enable = mkBoolOpt false "Whether or not to install and configure git.";
    userName = mkOpt types.str user.fullName "The name to configure git with.";
    userEmail = mkOpt types.str user.email "The email to configure git with.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [git commitizen cz-cli];
    environment.shellAliases = {
      git = "git -c color.ui=always";
      g = "git";

      gaa = "git add --all";
      gca = "git commit --amend";
      gcf = "git commit --fixup";
      gcm = "git commit -m";
      gco = "git checkout";
      gcp = "git cherry-pick";
      gcz = "git cz commit";
      gla = "git last";
      gll = "git ll";
      gp = "git push";
      gpf = "git push --force-with-lease";
      gpl = "git fetch -p && git rebase";
      gre = "git rebase";
      gst = "git status -sb";
      gun = "git unstage";
    };

    bobix.home.extraOptions = {
      programs.git = {
        enable = true;
        inherit (cfg) userName userEmail;
        lfs = enabled;
        extraConfig = {
          init = {defaultBranch = "main";};
          pull = {rebase = true;};
          push = {autoSetupRemote = true;};
          core = {whitespace = "trailing-space,space-before-tab";};
          safe = {
            directory = "${config.users.users.${user.name}.home}/.setup";
          };
          alias = {
            last = "log -1 HEAD --stat";
            ll = "log --pretty=format:'%C(yellow)%h%Cred%d %Creset%s%Cblue [%cn]' --decorate";
            st = "status -sb";
            undo = "reset --mixed HEAD^";
            unstage = "reset HEAD --";
          };
        };
      };
    };
  };
}
