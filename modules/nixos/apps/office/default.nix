{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.apps.office;
in {
  options.bobix.apps.office = with types; {
    enable = mkBoolOpt false "Whether or not to enable office.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [
      pkgs.onlyoffice-bin_latest
    ];
    bobix.system.fonts.fonts = with pkgs; [corefonts];
  };
}
