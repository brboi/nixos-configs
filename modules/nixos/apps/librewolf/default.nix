{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.apps.librewolf;
in {
  options.bobix.apps.librewolf = with types; {
    enable = mkBoolOpt false "Whether or not to enable librewolf.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      librewolf
    ];

    bobix.home = {
      file = {
      };

      extraOptions = {
        programs.librewolf = {
          enable = true;
          settings = {
            # see about:config or https://librewolf.net/docs/settings
            "browser.policies.runOncePerModification.extensionsInstall" = ''
              [
                "https://addons.mozilla.org/firefox/downloads/latest/bitwarden_password_manager/latest.xpi",
                "https://addons.mozilla.org/firefox/downloads/latest/canvasblocker/latest.xpi",
                "https://addons.mozilla.org/firefox/downloads/latest/ghostery/latest.xpi",
                "https://addons.mozilla.org/firefox/downloads/latest/privacy_badger/latest.xpi",
                "https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi"
              ]
            '';
            "browser.startup.homepage" = "https://start.duckduckgo.com/?kae=t&ko=1&ks=l&kak=-1&kao=-1&kaq=-1&kp=-2&kl=wt-wt&kav=1&k1=-1&kaj=m&kay=b&kax=-1&kap=-1&kau=-1&kt=Hack+Nerd+Font&km=m&kw=w&kj=3b4252&k7=2e3440&ku=1&ka=Hack+Nerd+Font&k18=1&kg=g";
            "general.autoScroll" = true;
            "media.autoplay.blocking_policy" = 2;
            "privacy.clearOnShutdown.downloads" = false;
            "privacy.clearOnShutdown.history" = false;
            "privacy.resistFingerprinting" = true;
            "webgl.disabled" = false;
          };
        };
      };
    };
  };
}
