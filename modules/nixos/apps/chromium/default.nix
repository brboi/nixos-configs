{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.apps.chromium;
in {
  options.bobix.apps.chromium = with types; {
    enable = mkBoolOpt false "Whether or not to enable chromium.";
  };

  config = mkIf cfg.enable {
    bobix.home = {
      extraOptions = {
        programs.chromium = {
          enable = true;
          package = pkgs.brave.override {
            commandLineArgs = [
              "--enable-features=VaapiVideoDecodeLinuxGL"
              "--ignore-gpu-blocklist"
              "--enable-zero-copy"
            ];
          };
          extensions = [
            {id = "cjpalhdlnbpafiamejdnhcphjbkeiagm";} # ublock origin
            {id = "mlomiejdfkolichcflejclcbmpeaniij";} # ghostery
            {id = "pkehgijcmpdhfbdbbnkijodmdjhbjlgp";} # privacy badger
            {id = "kcgpggonjhmeaejebeoeomdlohicfhce";} # cookie-remover
            {id = "nomnklagbgmgghhjidfhnoelnjfndfpd";} # canvas blocker
            {id = "nngceckbapebfimnlniiiahkandclblb";} # bitwarden
            {id = "ldbfanecnfeflgbmgkbmipobkmoolbjb";} # owl debug
            {id = "jllbemjkkabaohnjcnajhflahlkehmlf";} # odoo utility
            {id = "hmdmhilocobgohohpdpolmibjklfgkbi";} # odoo debug
            {
              # bypass paywalls
              id = "dcpihecpambacapedldabdbpakmachpb";
              updateUrl = "https://raw.githubusercontent.com/iamadamdev/bypass-paywalls-chrome/master/src/updates/updates.xml";
            }
          ];
        };
      };
    };
  };
}
