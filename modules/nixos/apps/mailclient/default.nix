{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.apps.mailclient;
in {
  options.bobix.apps.mailclient = with types; {
    enable = mkBoolOpt false "Whether or not to enable a mail client.";
  };

  config = mkIf cfg.enable {
    programs.geary.enable = true;
  };
}
