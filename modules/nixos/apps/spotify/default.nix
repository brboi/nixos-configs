{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.apps.spotify;
in {
  options.bobix.apps.spotify = with types; {
    enable = mkBoolOpt false "Whether or not to enable spotify.";
  };

  config = mkIf cfg.enable {
    networking.firewall.allowedTCPPorts = [57621];
    networking.firewall.allowedUDPPorts = [5353];
    environment.systemPackages = with pkgs; [spotify];
  };
}
