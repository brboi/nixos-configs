{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace}; let
  cfg = config.${namespace}.apps.ferdium;
in {
  options.${namespace}.apps.ferdium = with types; {
    enable = mkBoolOpt false "Whether or not to enable Ferdium.";
  };

  config = mkIf cfg.enable {environment.systemPackages = with pkgs; [ferdium];};
}
