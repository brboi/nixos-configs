{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.apps.belgian-eid;
in {
  options.bobix.apps.belgian-eid = with types; {
    enable = mkBoolOpt false "Whether or not to enable belgian-eid.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      eid-mw
    ];
    services.pcscd = {
      enable = true;
      plugins = with pkgs; [acsccid];
    };
  };
}
