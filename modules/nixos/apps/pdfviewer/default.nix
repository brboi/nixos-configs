{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.apps.pdfviewer;
in {
  options.bobix.apps.pdfviewer = with types; {
    enable = mkBoolOpt false "Whether or not to enable pdfviewer.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      libsForQt5.okular
      masterpdfeditor
    ];
  };
}
