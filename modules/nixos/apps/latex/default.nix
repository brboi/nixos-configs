{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.apps.latex;
in {
  options.bobix.apps.latex = with types; {
    enable = mkBoolOpt false "Whether or not to enable latex.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      texlive.combined.scheme-full
    ];
    bobix.system.fonts.fonts = with pkgs; [lmodern];
  };
}
