{
  options,
  config,
  pkgs,
  lib,
  inputs,
  namespace,
  ...
}:
with lib;
with lib.${namespace}; let
  cfg = config.${namespace}.security.thetis;
in {
  options.${namespace}.security.thetis = with types; {
    enable = mkBoolOpt false "Whether or not to enable Thetis Pro FIDO2 Key support.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      fido2-manage
    ];

    services.udev.extraRules = ''
      # Lock sessions on Thetis Pro removal.
      ACTION=="remove", ATTRS{idVendor}=="1ea8", ATTRS{idProduct}=="fc25", RUN+="${pkgs.systemd}/bin/loginctl lock-sessions"
    '';
    security.pam = {
      u2f = {
        enable = true;
        settings = {
          cue = true;
          authfile = ./u2f_keys;
        };
      };
      services = {
        login.u2fAuth = true;
        sudo.u2fAuth = true;
      };
    };
  };
}
