{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.security.wireguard;
in {
  options.bobix.security.wireguard = with types; {
    enable = mkBoolOpt false "Whether to enable wireguard.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      wireguard-tools
    ];

    networking.firewall = {
      allowedUDPPorts = [51821]; # Clients and peers can use the same port, see listenport
    };
  };
}
