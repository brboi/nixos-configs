{
  options,
  config,
  pkgs,
  lib,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.security.doas;
in {
  options.bobix.security.doas = {
    enable = mkBoolOpt false "Whether or not to replace sudo with doas.";
  };

  config = mkIf cfg.enable {
    # Disable sudo
    security.sudo.enable = false;

    # Enable and configure `doas`.
    security.doas = {
      enable = true;
      extraRules = [
        {
          users = [config.bobix.user.name];
          noPass = true;
          keepEnv = true;
        }
      ];
    };

    # Shim for the sudo command that utilizes doas.
    environment.systemPackages = with pkgs; [
      doas-sudo-shim
    ];

    # Add an alias to the shell for backward-compat and convenience.
    environment.shellAliases = {sudo = "doas";};
  };
}
