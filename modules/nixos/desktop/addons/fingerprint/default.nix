{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.desktop.addons.fingerprint;
in {
  options.bobix.desktop.addons.fingerprint = with types; {
    enable = mkBoolOpt false "Whether to enable the fingerprint support.";
  };

  config = mkIf cfg.enable {
    services.fprintd = {
      enable = true;
      tod = {
        enable = true;
        driver = pkgs.libfprint-2-tod1-goodix-550a;
      };
    };
    security.pam.services = {
      login.fprintAuth = true;
      sudo.fprintAuth = true;
    };
  };
}
