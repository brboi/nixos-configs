{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace}; let
  cfg = config.${namespace}.desktop.addons.system-monitor;
in {
  options.${namespace}.desktop.addons.system-monitor = with types; {
    enable = mkBoolOpt false "Whether to enable a system monitor GUI application.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      mission-center
    ];
  };
}
