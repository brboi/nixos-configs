{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace}; let
  cfg = config.${namespace}.desktop.addons.snipping-tool;
in {
  options.${namespace}.desktop.addons.snipping-tool = with types; {
    enable = mkBoolOpt false "Whether or not to enable snipping-tool.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      flameshot
    ];
  };
}
