{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.desktop.addons.arandr;
in {
  options.bobix.desktop.addons.arandr = with types; {
    enable = mkBoolOpt false "Whether to enable the arandr.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      arandr
    ];
  };
}
