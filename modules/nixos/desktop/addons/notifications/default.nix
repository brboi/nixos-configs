{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace}; let
  cfg = config.${namespace}.desktop.addons.notifications;
in {
  options.${namespace}.desktop.addons.notifications = with types; {
    enable = mkBoolOpt false "Whether to enable desktop notifications.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      dunst
      libnotify
    ];
  };
}
