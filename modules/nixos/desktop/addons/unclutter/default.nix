{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.desktop.addons.unclutter;
in {
  options.bobix.desktop.addons.unclutter = with types; {
    enable = mkBoolOpt false "Whether to enable unclutter.";
  };

  config = mkIf cfg.enable {
    services.unclutter-xfixes.enable = true;
  };
}
