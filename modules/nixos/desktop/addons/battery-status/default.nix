{
  options,
  config,
  pkgs,
  lib,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.desktop.addons.battery-status;
in {
  options.bobix.desktop.addons.battery-status = with types; {
    enable =
      mkBoolOpt false
      "Whether or not to add battery-status package";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs.bobix; [battery-status];
  };
}
