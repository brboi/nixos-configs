{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.desktop.addons.lockscreen;
in {
  options.bobix.desktop.addons.lockscreen = with types; {
    enable = mkBoolOpt false "Whether to enable screen lock utility.";
  };

  config = mkIf cfg.enable {
    programs.slock.enable = true;
  };
}
