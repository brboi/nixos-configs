{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace}; let
  cfg = config.${namespace}.desktop.addons.login-manager;
in {
  options.${namespace}.desktop.addons.login-manager = with types; {
    enable = mkBoolOpt false "Whether to enable a login manager.";
  };

  config = mkIf cfg.enable {
    services.greetd = {
      enable = true;
      settings = rec {
        initial_session = {
          command = "${pkgs.hyprland}/bin/Hyprland";
          user = "boi"; # FIXME
        };
        default_session = initial_session;
      };
    };
  };
}
