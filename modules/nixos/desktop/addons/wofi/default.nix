{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace}; let
  cfg = config.${namespace}.desktop.addons.wofi;
in {
  options.${namespace}.desktop.addons.wofi = with types; {
    enable = mkBoolOpt false "Whether or not to enable wofi.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      wofi
    ];
  };
}
