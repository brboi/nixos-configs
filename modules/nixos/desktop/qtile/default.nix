{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.desktop.qtile;
in {
  options.bobix.desktop.qtile = with types; {
    enable = mkBoolOpt false "Whether or not to enable qtile WM.";
  };

  config = mkIf cfg.enable {
    services.xserver.windowManager.qtile = {
      enable = true;
      extraPackages = python3Packages:
        with python3Packages; [
          qtile-extras
        ];
    };

    services.xserver.windowManager.session = [
      {
        name = "qtile";
        start = ''
          qtile start -b x11 &
          waitPID=$!
        '';
      }
    ];
  };
}
