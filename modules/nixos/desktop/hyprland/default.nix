{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace}; let
  cfg = config.${namespace}.desktop.hyprland;
in {
  options.${namespace}.desktop.hyprland = with types; {
    enable = mkBoolOpt false "Whether or not to enable hyprland WM.";
  };

  config = mkIf cfg.enable {
    programs.hyprland = enabled;

    ${namespace} = {
      desktop.addons = {
        clipboard = enabled;
        electron-support = enabled;
        gtk = enabled;
        wallpapers = enabled;
        wofi = enabled;
      };
    };

    services.upower = enabled;
    programs.dconf = enabled;
    programs.waybar = enabled;
    environment.systemPackages = with pkgs; [
      brightnessctl
      hypridle
      hyprlock
      hyprpaper
      lxqt.lxqt-policykit
      wev

      # https://wiki.hyprland.org/Useful-Utilities/Must-have/#qt-wayland-support
      libsForQt5.qt5.qtwayland
      kdePackages.qtwayland

      # for screenshots TODO move in a custom screenshot package, maybe?
      slurp
      grim
      imagemagick
      swappy
      wl-clipboard
    ];
  };
}
