{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.bobix; let
  cfg = config.bobix.remotes.circolo;
in {
  options.bobix.remotes.circolo = with types; {
    enable = mkBoolOpt false "Whether or not to enable circolo remote configuration.";
  };

  config = mkIf cfg.enable {
    # Enable WireGuard
    networking.wireguard.interfaces = {
      # "circolo" is the network interface name. You can name the interface arbitrarily.
      circolo = {
        # Determines the IP address and subnet of the client's end of the tunnel interface.
        ips = ["10.0.0.2/24"];
        listenPort = 51821; # to match firewall allowedUDPPorts (without this wg uses random port numbers)

        # Path to the private key file.
        #
        # Note: The private key can also be included inline via the privateKey option,
        # but this makes the private key world-readable; thus, using privateKeyFile is
        # recommended.
        privateKeyFile = "$XDG_CONFIG_HOME/wireguard/client_private_key";

        peers = [
          # For a client configuration, one peer entry for the server will suffice.

          {
            # Public key of the server (not a file path).
            publicKey = "F/oo4+h3p0UDxl6Y+QHXDEly3VTpHr66UED+GtQXoXI=";

            # Forward all the traffic via VPN.
            allowedIPs = ["0.0.0.0/0" "::/0"];
            # Or forward only particular subnets
            #allowedIPs = [ "10.100.0.1" "91.108.12.0/22" ];

            # Set this to the server IP and port.
            endpoint = "www.quattromori.be:51821"; # ToDo: route to endpoint not automatically configured https://wiki.archlinux.org/index.php/WireGuard#Loop_routing https://discourse.nixos.org/t/solved-minimal-firewall-setup-for-wireguard-client/7577

            # Send keepalives every 25 seconds. Important to keep NAT tables alive.
            persistentKeepalive = 25;
          }
        ];
      };
    };
  };
}
