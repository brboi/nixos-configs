{channels, ...}: final: prev: {
  inherit (channels.unstable) qtile qtile-unwrapped qtile-extras;
}
