# Sources Of Inspiration

This is heavily inspired/copied from following sources.

Special thanks to them!

- Jake Hamilton
  - https://github.com/jakehamilton/config
- Wil Taylor
  - https://github.com/wiltaylor
- Matthias Benaets
  - https://github.com/MatthiasBenaets
- Ion Mudreac
  - https://mudrii.medium.com/nixos-native-flake-deployment-with-luks-drive-encryption-and-lvm-b7f3738b71ca

# Disclaimer

> This is my personal config, tailored for my needs and hardware only.
>
> It is not guaranteed it will work properly on your side.

# Devenv Update

```sh
cd my-flake-repo
devenv update
git add .
git commit -m "bump(devenv): update"
```

# System Update

```sh
cd my-flake-repo
nix flake update
git add .
git commit -m "bump(flake): update"
nixos-rebuild switch --use-remote-sudo --flake .#
```

# NixOS Installation Guide

- Boot up on a fresh NixOS ISO (i.e. the version with graphical installer)
- Open a terminal

## Keyboard Layout

```console
# I use Belgian AZERTY Standard Layout

# Set keyboard layout for the console. See 'man loadkeys' to find other keymaps.
loadkeys be-latin1

# Eventually, set the keyboard layout for X server
setxkbmap be
```

## Networking

- Plug a network cable, or set up wireless network:

```console
# Stop network manager to configure manually
systemctl stop NetworkManager

# Start wpa_supplicant
systemctl start wpa_supplicant

# Run wpa CLI
wpa_cli

# Then enter the following commands (without the '>')
> add_network
0
> set_network 0 ssid "myhomenetwork"
OK
> set_network 0 psk "mypassword"
OK
> set_network 0 key_mgmt WPA_PSK
OK
> enable_network 0
OK
> quit
```

## Partitioning

- Partition Labels:
  - Boot = "boot"
  - LVM = "root"
- Partition Size:
  - Boot = 512MiB
  - LVM = Rest

```console
export ROOT_DISK=/dev/nvme0
parted -a opt --script ${ROOT_DISK} \
    mklabel gpt \
    mkpart primary fat32 0% 513MiB \
    mkpart primary 513MiB 100% \
    set 1 esp on \
    name 1 boot \
    set 2 lvm on \
    name 2 root

# Review the partitioning
fdisk ${ROOT_DISK} -l
```

## Encrypt Primary Disk

```console
export SWAP_SIZE=10G

# Encrypt root partition. Make sure you use a secure password.
cryptsetup luksFormat /dev/disk/by-partlabel/root

# Open encrypted partition.
cryptsetup luksOpen /dev/disk/by-partlabel/root luksroot
```

## Setting Up LVM

```console
# Create a physical volume
pvcreate /dev/mapper/luksroot

# Create a volume group from /dev/mapper/luksroot
vgcreate vg /dev/mapper/luksroot

# Create a logical volume for swap and another for root partition.
lvcreate -L ${SWAP_SIZE} -n swap vg
lvcreate -l '100%FREE' -n root vg

# Review volumes and partitions created
lvdisplay
```

## Format Disks

```console
mkfs.fat -F 32 -n boot /dev/disk/by-partlabel/boot
mkfs.ext4 -L root /dev/vg/root
mkswap -L swap /dev/vg/swap
```

## Installation

- Partition with label ... to ...
  - "root" -> `/mnt`
  - "boot" -> `/mnt/boot`

```console
# mount /dev/disk/by-label/root /mnt
# mkdir -p /mnt/boot
# mount /dev/disk/by-label/boot /mnt/boot
# swapon /dev/vg/swap
```

### Quickly Apply This Flake From Internet...

- You may append `#<host>` to the following command, with the host you want to apply (below example shows `#laptop`).

```console
# nix-shell -p git nixFlakes
# nixos-install --root /mnt  --flake gitlab:brboi/nixos-configs/master#laptop
# reboot
```

### ...or Clone This Repo, Fine Tune The Config & Apply The Flake

- Generate initial configuration (optional for me: it is mostly to create `hardware-configuration.nix`)
- Clone this repo into a subfolder inside your future `/etc/nixos` folder. You could move it after rebooting in your new system.
- Copy `hardware-configuration.nix` into `/etc/nixos/<clonedrepo>/systems/x86_64-linux/<host>/hardware.nix` where `<host>` is yours. (optional if an existing host already fits, i.e. for me)
- You should also add `/etc/nixos/<clonedrepo>/systems/x86_64-linux/<host>/default.nix`, copy it from another machine. Then adapt both files.

```console
nixos-generate-config --root /mnt
nix-env -iA nixos.git
git clone --branch master https://gitlab.com/brboi/nixos-configs.git /mnt/etc/nixos/nixos-configs
mkdir /mnt/etc/nixos/nixos-configs/systems/x86_64-linux/<host>
cp /mnt/etc/nixos/hardware-configuration.nix /mnt/etc/nixos/nixos-configs/systems/x86_64-linux/<host>/hardware.nix
```

WARN: the above is unfinished and not quite good.
I recommend cloning the repo after the hardware config has been generated, then adapt the repo's flake, then:

```console
nixos-install --root /mnt --flake /mnt/etc/nixos/nixos-configs#<host>
```

## After Installation

- Set a root password after installation is done
- Reboot
- Login
  - If initialPassword is not set use TTY:
    - Ctrl - Alt - F1
    - login as root
      ```console
      passwd <user>
      ```
    - Ctrl - Alt - F7
    - login as user
- Then:
  - `sudo mv <location of cloned directory>/* ~/.setup`
  - `sudo chown -R <user>:users ~/.setup`
  - `sudo rm /etc/nixos/configuration.nix` - This is done because in the past it would auto update this config if you would have auto update in your configuration.
  - or just clone flake again do apply same changes.
- Dual boot:
  - OSProber probably did not find your Windows partition after the first install
  - There is a high likelihood it will find it after:
    - `sudo nixos-rebuild switch --flake ~/.setup#<host>`
- Rebuilds:
  - `sudo nixos-rebuild switch --flake ~/.setup#<host>`
  - For example `sudo nixos-rebuild switch --flake ~/.setup#lenovo`
