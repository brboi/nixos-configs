{
  lib,
  pkgs,
  config,
  osConfig ? {},
  format ? "unknown",
  ...
}:
with lib.bobix; {
  bobix = {
    user = {
      enable = true;
      name = config.snowfallorg.user.name;
    };

    cli-apps = {
      home-manager = enabled;
    };

    desktop = {
      qtile = enabled;
      hyprland = enabled;
      xsession = enabled;
      addons = {
        dunst = enabled;
        rofi = enabled;
        waybar = enabled;
      };
    };

    tools = {
      ssh = enabled;
    };
  };

  home.sessionPath = [
    "$HOME/bin"
  ];

  home.stateVersion = "23.05";
}
