{pkgs, ...}: {
  enterShell = ''
    git --version
  '';

  # https://devenv.sh/languages/
  languages.nix.enable = true;

  # https://devenv.sh/pre-commit-hooks/
  pre-commit.hooks.alejandra.enable = true;
  pre-commit.hooks.black.enable = true;
  pre-commit.hooks.commitizen.enable = true;
  pre-commit.hooks.prettier.enable = true;
  pre-commit.hooks.shellcheck.enable = true;

  # https://devenv.sh/processes/
  # processes.ping.exec = "ping example.com";

  # See full reference at https://devenv.sh/reference/options/
}
