{
  config,
  lib,
  pkgs,
  modulesPath,
  inputs,
  ...
}:
with lib;
with lib.bobix; {
  imports = with inputs.nixos-hardware.nixosModules; [
    (modulesPath + "/installer/scan/not-detected.nix")
    lenovo-thinkpad-l14-amd
  ];

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";

  boot = {
    kernelModules = ["kvm-amd"];
    kernelPackages = pkgs.linuxPackages_zen;

    initrd = {
      availableKernelModules = ["nvme" "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" "sdhci_pci"];
      kernelModules = ["kvm-amd"];

      # https://nixos.org/manual/nixos/stable/#sec-luks-file-systems-fido2-systemd
      systemd = enabled;
      luks.devices = {
        crypt = {
          crypttabExtraOpts = ["fido2-device=auto"];
          device = "/dev/nvme0n1p2";
          preLVM = true;
        };
      };
    };

    extraModulePackages = [];

    loader = {
      efi = {
        efiSysMountPoint = "/boot";
        canTouchEfiVariables = true;
      };

      grub = {
        enable = true;
        efiSupport = true;
        enableCryptodisk = true;
        device = "nodev";
        configurationLimit = 10;
      };
    };
  };

  services.blueman = enabled;
  services.autorandr = enabled;
  services.libinput = {
    enable = true;
    touchpad.naturalScrolling = true;
  };
  services.displayManager.logToJournal = true;
  services.xserver = {
    videoDrivers = ["amdgpu"];
    deviceSection = ''
      Option "TearFree" "true"
    '';
    enable = true;
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/root";
      fsType = "ext4";
      options = ["noatime" "nodiratime"];
    };
    "/boot" = {
      device = "/dev/disk/by-label/boot";
      fsType = "vfat";
    };
  };

  swapDevices = [{device = "/dev/disk/by-label/swap";}];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";

  hardware = {
    cpu.amd.updateMicrocode = true;
    bluetooth = enabled;
    graphics = {
      enable = true;
      extraPackages = with pkgs; [
        intel-media-driver
        intel-vaapi-driver
      ];
    };
  };
  environment.sessionVariables = {LIBVA_DRIVER_NAME = "iHD";};

  # Enable all unfree hardware support.
  hardware.firmware = with pkgs; [firmwareLinuxNonfree];
  hardware.enableAllFirmware = true;
  hardware.enableRedistributableFirmware = true;

  environment.systemPackages = with pkgs; [
    microcodeAmd

    glxinfo
    radeontop
    vulkan-headers
    vulkan-loader
    vulkan-tools
  ];

  systemd.services = {
    systemd-udev-settle.enable = false; # helps for faster boot
    NetworkManager-wait-online.enable = false; # no need to wait for network to continue booting
  };

  # Enable DHCP on the wireless link
  networking = {
    wireless.interfaces = ["wlp6s0"];
    interfaces."wlp6s0" = {useDHCP = true;};
    useDHCP = false; # should be false
    dhcpcd.wait = "background"; # no need to wait interfaces to have an IP to continue booting
    dhcpcd.extraConfig = "noarp"; # avoid checking if IP is already taken to boot a few seconds faster
  };
}
