{
  pkgs,
  lib,
  ...
}:
with lib;
with lib.bobix; {
  imports = [./hardware.nix];

  bobix = {
    suites = {
      common = enabled;
      desktop = enabled;
      development = enabled;
      games = enabled;
      social = enabled;
    };

    desktop.addons = {
      fingerprint = enabled;
    };

    remotes = {
      # circolo = enabled; FIXME the wireguard client setup is incomplete
    };
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05";
}
